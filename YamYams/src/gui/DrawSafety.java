package gui;

import javax.swing.JComponent;
import javax.swing.JProgressBar;

import computation.Main;
import computation.SafetyOfAllSquares;

public class DrawSafety extends Thread {
	
	public SafetyOfAllSquares safetyThread;
	
	public Canvas.Safety safetyPanel;
	
	public JProgressBar progressBar;
	
	public JComponent[] onOffComponents;

	public DrawSafety(SafetyOfAllSquares safetyThread, Canvas.Safety safetyPanel, JProgressBar progressBar, JComponent... onOffComponents ){
		this.safetyPanel = safetyPanel;
		this.safetyThread = safetyThread;
		this.progressBar = progressBar;
		this.onOffComponents = onOffComponents;
	}
	
	public void run(){
		if(!safetyThread.isAlive()){
			safetyThread.start();
		}
		for(JComponent jc : onOffComponents){
			jc.firePropertyChange("threadActivity", false, true) ;
		}
		while(safetyThread.isAlive()){
			try {
				updateComponents();
				sleep(50);
			} catch (InterruptedException e) {
				safetyThread.interrupt();
				break;
			}
		}
		updateComponents();
		progressBar.setValue(100);
		for(JComponent jc : onOffComponents){
			jc.firePropertyChange("threadActivity", true, false) ;
		}
	}
	
	public void updateComponents(){
		Main.setSafetySquares(safetyThread.getSafetyOfAllSquares());
		//System.out.println(safetyThread.getSafetyOfAllSquares());
		safetyPanel.repaint();
		progressBar.setValue((int) (safetyThread.getProgress() * (double) 100));
	}
	
}
