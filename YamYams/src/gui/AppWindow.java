package gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.LineBorder;

import computation.Main;
import computation.SafetyOfAllSquares;
import computation.Square;
import computation.YamYam;

import java.awt.Point;

import javax.swing.JLayeredPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import java.awt.event.MouseMotionAdapter;
import java.awt.Toolkit;
import javax.swing.JRadioButton;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JTabbedPane;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import net.miginfocom.swing.MigLayout;
import java.awt.Font;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.FlowLayout;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.JProgressBar;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class AppWindow extends JFrame {

	private static final long serialVersionUID = 165345234231L;
	
	/**
	 * Oberfl�chenkomponenten
	 */
	private JPanel contentPane;
	private Canvas.World world_panel;
	private Canvas.YamYam yamyam_panel;
	private Canvas.Safety safety_panel;
	private Canvas.Reachable reachable_panel;
	private JLayeredPane canvasPane;
	private ButtonGroup auswahlGruppe;
	private Set<JRadioButton> auswahlButtons;
	
	
	/**
	 *  Der ReadWorld Dialog 
	*/
	private ReadWorld feld_einlesen_dialog;
	private JTabbedPane tabbedPane;
	private JPanel worldPanel;
	private JRadioButton zeigeSicherheit;
	private JButton readWorld;
	private JRadioButton alleErreichbarenFelder;
	private JPanel graphicPanel;
	private JPanel panel_1;
	private JSpinner spinnerAccuracy;
	private JCheckBox chckbxAccuracy;
	private JLabel aktuelleSicherheitLabel;
	private JTextField aktuelleSicherheitFeld;
	private JPanel panel_2;
	
	/**
	 * F�r wie viele Schritte soll die Ausgangserreichwahrscheinlichkeit unsicherer Felder berechnet werden? Also wie genau?
	 * 0 => Ausgangserreichwahrscheinlichkeit wird auf 0.5 gesetzt
	 */
	private int accuracy = 1000;
	private JProgressBar progressBar;



	/**
	 * Create the frame.
	 */
	public AppWindow(Runnable runnable) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(AppWindow.class.getResource("/icon.png")));
		setTitle("Torkelnde YamYams");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 273, 242);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		feld_einlesen_dialog = new ReadWorld(this);
		
		canvasPane = new JLayeredPane();
		contentPane.add(canvasPane, BorderLayout.CENTER);
		canvasPane.setLayout(null);
		
		yamyam_panel = new Canvas.YamYam();
		yamyam_panel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				Point mousePosition = arg0.getPoint();
				Point nextFeldPosition = new Point(
						(int) (mousePosition.getX() / Square.getSquareDimensions().getWidth()),
						(int) (mousePosition.getY() / Square.getSquareDimensions().getHeight()));
				YamYam.setCoords(nextFeldPosition);
				yamyam_panel.repaint();
				
				updateSafetyField();
			}
		});
		yamyam_panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				Point mousePosition = arg0.getPoint();
				Point nextFeldPosition = new Point(
						(int) (mousePosition.getX() / Square.getSquareDimensions().getWidth()),
						(int) (mousePosition.getY() / Square.getSquareDimensions().getHeight()));
				YamYam.setCoords(nextFeldPosition);
				yamyam_panel.repaint();
				reachable_panel.repaint();
				
				updateSafetyField();
			}
		});
		yamyam_panel.setToolTipText("Halten sie die Maus gedr\u00FCckt um das YamYam umher zu ziehen");
		yamyam_panel.setBounds(0, 0, 1, 33);
		canvasPane.add(yamyam_panel);
		yamyam_panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		yamyam_panel.setBackground(new Color(0, 0, 0, 0));
		yamyam_panel.setLayout(null);
		
		reachable_panel = new Canvas.Reachable();
		reachable_panel.setBounds(0, 0, 10, 10);
		canvasPane.add(reachable_panel);
		reachable_panel.setVisible(false);
		
		safety_panel = new Canvas.Safety();
		safety_panel.setBounds(0, 0, 10, 10);
		canvasPane.add(safety_panel);
		safety_panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		safety_panel.setBackground(new Color(0, 0, 0, 0));
		safety_panel.setLayout(null);
		safety_panel.setVisible(true);
		
		world_panel = new Canvas.World();
		world_panel.setBounds(0, 0, 1, 33);
		canvasPane.add(world_panel);
		world_panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		world_panel.setBackground(Color.WHITE);
		world_panel.setLayout(null);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 11));
		contentPane.add(tabbedPane, BorderLayout.EAST);
		
		worldPanel = new JPanel();
		tabbedPane.addTab("Welteinstellungen", null, worldPanel, null);
		
		zeigeSicherheit = new JRadioButton("Zeige die Sicherheit aller Felder an");
		zeigeSicherheit.setToolTipText("Es wird angezeigt, von welchen Feldern mit 100%er (gr\u00FCn), mit 0%er (rot) oder anderer \r\nWarscheinlichkeit (gelb) ein Ausgang erreicht wird");
		zeigeSicherheit.setSelected(true);
		zeigeSicherheit.setActionCommand("safety");
		
		alleErreichbarenFelder = new JRadioButton("Zeige alle erreichbaren Felder an ");
		alleErreichbarenFelder.setToolTipText("Es werden alle Felder markiert, welche von der aktuellen Position des YamYams aus erreichbar sind");
		alleErreichbarenFelder.setActionCommand("reachable");
		
		//Erzeuge einen Actionlistener f�r die Auswahlgruppe
		ActionListener auswahlGruppeListener = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				reachable_panel.setVisible(false);
				safety_panel.setVisible(false);
				switch(arg0.getActionCommand()){
				case "reachable":
					reachable_panel.setVisible(true);
					break;
				case "safety":
					safety_panel.setVisible(true);
					break;
				}
			}
		};
		//Gruppiere die Auswahlm�glichkeiten
		auswahlGruppe = new ButtonGroup();
		auswahlButtons = new HashSet<JRadioButton>();
		auswahlButtons.add(zeigeSicherheit);
		auswahlButtons.add(alleErreichbarenFelder);
		for(JRadioButton button : auswahlButtons){
			auswahlGruppe.add(button);
			button.addActionListener(auswahlGruppeListener);
		}
		
		readWorld = new JButton("Welt einlesen");
		readWorld.setToolTipText("\u00D6ffnet einen Dialog, in dem sie ein Square eingeben k\u00F6nnen.");
		readWorld.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//�ffne das Fenster "Square einlesen"
				feld_einlesen_dialog.setVisible(true);
			}
		});
		worldPanel.setLayout(new MigLayout("", "[206px,grow]", "[23px,top][23px,top][23px,top][top][top][][]"));
		worldPanel.add(readWorld, "cell 0 0,growx,aligny top");
		worldPanel.add(alleErreichbarenFelder, "cell 0 1,growx,aligny top");
		worldPanel.add(zeigeSicherheit, "cell 0 2,growx,aligny top");
		
		panel_1 = new JPanel();
		panel_1.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName().equals("threadActivity")){
					if((boolean) arg0.getNewValue()){
						chckbxAccuracy.setEnabled(false);
						spinnerAccuracy.setEnabled(false);
					}
					else{
						chckbxAccuracy.setEnabled(true);
						spinnerAccuracy.setEnabled(true);
					}
				}
			}
		});
		panel_1.setToolTipText("Berechne die Wahrscheinlichkeit nach x Schritten auf einem sicheren Feld / Ausgang zu stehen");
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		worldPanel.add(panel_1, "cell 0 3,growx,aligny top");
		
		chckbxAccuracy = new JCheckBox("Genauigkeit");
		chckbxAccuracy.setSelected(true);
		chckbxAccuracy.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.DESELECTED){
					spinnerAccuracy.setEnabled(false);
					setAccuracy(0);
				}
				else{
					spinnerAccuracy.setEnabled(true);
					setAccuracy((int) spinnerAccuracy.getValue());
				}
			}
		});
		panel_1.add(chckbxAccuracy);
		
		spinnerAccuracy = new JSpinner();
		spinnerAccuracy.setPreferredSize(new Dimension(100, 20));
		spinnerAccuracy.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent arg0) {
				setAccuracy((int) spinnerAccuracy.getValue());
			}
			
		});
		spinnerAccuracy.setToolTipText("Die Anzahl von Schritten, die simuliert werden um die Ausgangserreichwahrscheinlichkeit zu ermitteln");
		spinnerAccuracy.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(0), null, new Integer(100)));
		panel_1.add(spinnerAccuracy);
		
		panel_2 = new JPanel();
		panel_2.setToolTipText("Die Ausgangserreichwahrscheinlichkeit des Feldes, auf dem das Yamyam sich befindet");
		worldPanel.add(panel_2, "flowx,cell 0 4,growx,aligny top");
		FlowLayout fl_panel_2 = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel_2.setLayout(fl_panel_2);
		
		aktuelleSicherheitLabel = new JLabel("Sicherheit des aktuellen Feldes");
		aktuelleSicherheitLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		panel_2.add(aktuelleSicherheitLabel);
		aktuelleSicherheitLabel.setToolTipText("");
		
		aktuelleSicherheitFeld = new JTextField();
		aktuelleSicherheitFeld.setBackground(Color.WHITE);
		aktuelleSicherheitFeld.setEditable(false);
		panel_2.add(aktuelleSicherheitFeld);
		aktuelleSicherheitFeld.setColumns(5);
		panel_2.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{aktuelleSicherheitLabel, aktuelleSicherheitFeld}));
		
		progressBar = new JProgressBar();
		progressBar.setToolTipText("Der Fortschritt der Ausgangserreichwahrscheinlichkeitsberechnung");
		worldPanel.add(progressBar, "cell 0 5,growx");
		graphicPanel = new JPanel();
		tabbedPane.addTab("Grafikeinstellungen", null, graphicPanel, null);
		
		JCheckBox chckbxBlauStattRot = new JCheckBox("Blau statt Rot");
		chckbxBlauStattRot.setToolTipText("F\u00E4rbe gef\u00E4hrliche Felder blau statt rot");
		chckbxBlauStattRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				safety_panel.setBlue(!safety_panel.getBlue());
			}
		});
		graphicPanel.setLayout(new MigLayout("", "[150px:n]", "[23px][23px]"));
		graphicPanel.add(chckbxBlauStattRot, "cell 0 0,alignx left,aligny top");
		
		JComboBox<String> colorDistributionFunction = new JComboBox<String>();
		String[] colorDistributionPossibilities = {"Lineare Farbverteilung", "Logistische Farbverteilung"};
		colorDistributionFunction.setModel(new DefaultComboBoxModel<String>(colorDistributionPossibilities));
		colorDistributionFunction.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					for(int i = 0; i < colorDistributionPossibilities.length;i++){
						if(colorDistributionPossibilities[i] == e.getItem()){
							Canvas.Safety.setFunctionType(i + 1);
							safety_panel.repaint();
						}
					}
				}
			}
			
		});
		graphicPanel.add(colorDistributionFunction, "cell 0 1,alignx left,aligny top");	
		
		YamYam.setCoords(new Point(1, 1));
		
		pack();
	}
	
	/**
	 * Methode um die Sichtbarkeit des Einlesedialogs zu kontrollieren
	 * @param b True f�r Sichtbarkeit, False f�r Schlie�en
	 */
	public void setFeldDialogVisible (boolean b){
		feld_einlesen_dialog.setVisible(b);
	}
	
	/**
	 * Methode um die String-Repr�sentation des Feldes zu empfangen und weiter zu leiten
	 * @param feld Die Textrepr�sentation des Feldes
	 */
	public void visualizeBrett(){
		canvasPane.setPreferredSize(Canvas.getCanvasSize());
		canvasPane.setSize(Canvas.getCanvasSize());
		pack();
		//Setze die neue  Gr��e und Male die Canvas erneut
		JComponent[] panels = {	world_panel,
								safety_panel,
								reachable_panel,
								yamyam_panel};
		for(JComponent panel : panels){
			panel.setSize(Canvas.getCanvasSize());
			panel.repaint();
		}
		drawSafety();
	}
	
	public void updateSafetyField(){
		Double aktuelleSicherheit = new Double(0);
		for(Double safety : Main.getSafetySquares().keySet()){
			if( Main.getSafetySquares().get(safety).contains(YamYam.getSquare())){
				aktuelleSicherheit = safety;
				break;
			}
		}
		String aktuelleSicherheitString = aktuelleSicherheit.toString();
		if(aktuelleSicherheitString.length() > 8){
			aktuelleSicherheitString = aktuelleSicherheitString.substring(0, 8);
		}
		aktuelleSicherheitFeld.setText(aktuelleSicherheitString);
	}
	
	public void drawSafety(){
		//Erzeuge einen neuen safetyThread um alle Sicherheiten zu berechnen
		SafetyOfAllSquares safetyThread = new SafetyOfAllSquares(accuracy);
		//Und erzeuge einen DrawSafety, der das Sicherheitscanvas updatet
		DrawSafety drawSafety = new DrawSafety(safetyThread, safety_panel, progressBar, panel_1);
		drawSafety.start();
	}
	
	/**
	 * �ndert die Genauigkeit und unterscheidet sich der Wert vom vorherigen Wert, so wird das Brett neu gemalt
	 * @param accuracy F�r wie viele Schritte soll die Ausgangserreichwahrscheinlichkeit unsicherer Felder berechnet werden? Also wie genau?
	 * 0 => Ausgangserreichwahrscheinlichkeit wird auf 0.5 gesetzt
	 */
	public void setAccuracy(int accuracy){
		if(this.accuracy != accuracy){
			this.accuracy = accuracy;
			drawSafety();
		}
	}
}
