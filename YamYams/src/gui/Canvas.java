package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Map;
import java.util.Set;

import javax.swing.JComponent;

import computation.Main;
import computation.Square;

public abstract class Canvas {

	/**
	 * Gibt die optimale Gr��e der Leinwand zur�ck
	 * @return Die optimale Gr��e der Leinwand
	 */
	public static Dimension getCanvasSize(){
		if(computation.World.getWorld() != null){
			return new Dimension(
					(int) ( computation.World.getWorld()[0].length * Square.getSquareDimensions().getWidth() ), 
					(int) ( computation.World.getWorld().length * Square.getSquareDimensions().getHeight())
					);
		}else return new Dimension(0,0);
	}
	
	/**
	 * Malt ein Square an die �bergebene Position auf der Welt
	 * @param g Die Graphics Komponente zum malen
	 * @param coords Die Koordinaten des Feldes auf der Welt
	 */
	public static void drawSquare(Graphics g, Point coords){
		g.fillRect(
				(int) (coords.getX() * Square.getSquareDimensions().getWidth()),
				(int) (coords.getY() * Square.getSquareDimensions().getHeight()),
				(int) (Square.getSquareDimensions().getWidth()),
				(int) (Square.getSquareDimensions().getHeight())
				);
	}
	
	/**
	 * Malt das �bergebene Square an die �bergebene Position auf der Welt
	 * @param g Die Graphics Komponente zum malen
	 * @param square Das Feld auf der Welt
	 */
	public static void drawSquare(Graphics g, Square square){
		Canvas.drawSquare(g, square.getCoords());
	}
	
	/**
	 * Malt alle �bergebenen Squares an die �bergebene Position auf der Welt
	 * @param g Die Graphics Komponente zum malen
	 * @param squares Die Felder auf der Welt
	 */
	public static void drawSquares(Graphics g, Square[] squares){
		for(Square square : squares){
			Canvas.drawSquare(g, square.getCoords());
		}
	}
	
	/**
	 * F�gt die �bergebene Farbe und den Alpha-Wert zusammen
	 * @param color Die Farbe, der ein Alpha-Wert gegeben werden soll
	 * @param alpha Der neue Alpha-Wert
	 * @return Die Farbe mit dem Alpha-Wert
	 */
	public static Color setAlpha(Color color, int alpha){
		return new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha);
	}
	
	/**
	 * Die Leinwand um das World darzustellen
	 * @author Niels
	 *
	 */
	public static class World extends JComponent {
		
		public World() {
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 164653543L;
		
		public void paint(Graphics g){
			if(computation.World.getWorld() != null){
					for(int i = 0; i < computation.World.getWorld().length; i++){
						for(int j = 0; j < computation.World.getWorld()[i].length; j++){
							g.setColor(computation.World.getWorld()[i][j].getColor());
							Canvas.drawSquare(g, new Point(j, i));
						}
					}
			}
		}

	}

	/**
	 * Die Leinwand um die T�tigkeiten des YamYams darzustellen
	 * @author Niels
	 *
	 */
	public static class YamYam extends JComponent {
		
		/**
		 * Wieviel Prozent der Zellengr��e soll das YamYam lang/breit sein?
		 */
		double percOfCell;
		

		
		/**
		 * Standart Konstruktor<br>
		 * Erreichbare Felder werden gekennzeichnet<br>
		 * Das YamYam hat 75% der Zellenl�nge/-breite
		 */
		public YamYam() {
			this(0.75);
		}

		
		/**
		 * 
		 * @param b Sollen die erreichbaren Felder gekennzeichnet werden?
		 * @param perc Wieviel Prozent der Zellengr��e soll das YamYam lang/breit sein?
		 */
		public YamYam(double perc) {
			setPercOfCell(perc);
		}

		private static final long serialVersionUID = 4654353423322L;
		
		public void paint(Graphics g){
			g.setColor(new Color(255, 0 , 0));
			// Ist der Kreis nur Perc mal so lang wie die ihn beinhaltende Zelle,
			// so ist der Abstand vom Rand der Zelle f�r eine Mittige Platzierung 
			// ( Zellenl�nge * (1- Perc) ) / 2
			g.fillArc(
					(int) (computation.YamYam.getCoords().getX() * Square.getSquareDimensions().getWidth() + ( Square.getSquareDimensions().getWidth() * (1 - percOfCell) / 2) ), 
					(int) (computation.YamYam.getCoords().getY() * Square.getSquareDimensions().getHeight() + ( Square.getSquareDimensions().getHeight() * (1 - percOfCell) / 2)), 
					(int) (Square.getSquareDimensions().getWidth() * percOfCell),
					(int) (Square.getSquareDimensions().getHeight() * percOfCell), 
					0, 
					360);
		}
		
		
		/**
		 * 
		 * @param perc Wieviel Prozent der Zellengr��e soll das YamYam lang/breit sein?
		 */
		public void setPercOfCell(double perc){
			percOfCell = perc;
		}
		
		/**
		 * Wieviel Prozent der Zellengr��e ist das YamYam lang/breit?
		 */
		public double getPercOfCell(){
			return percOfCell;
		}
	}
	
	public static class Reachable extends JComponent {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -8479536011949620282L;
		/**
		 * Der Alphawert der gemalten Overlays
		 */
		private static int alpha = 100;
		
		public Reachable(){
			
		}
		
		public void paint(Graphics g){
			g.setColor(new Color(0, 0, 255, alpha));
			Square aktuellesFeld = computation.World.getSquareAt(computation.YamYam.getCoords());
			if(aktuellesFeld != null){
				Set<Square> erreichbareFelder = aktuellesFeld.getYamyamAllReachableSquares();
				if(erreichbareFelder != null){
					Canvas.drawSquares(g, erreichbareFelder.toArray(new Square[0]));
				}
			}
		}
		
		/**
		 * 
		 * @param alpha Der Alphawert der gemalten Overlays
		 */
		public static void setAlpha(int alpha){
			Reachable.alpha = alpha;
		}
		
		/**
		 * 
		 * @return Der Alphawert der gemalten Overlays
		 */
		public static int getAlpha(){
			return Reachable.alpha;
		}
		
	}
	
	
	/**
	 * Die Leinwand um die Safety der Felder darzustellen
	 * @author Niels
	 */
	public static class Safety extends JComponent {

		private static final long serialVersionUID = 15643523121325L;
		
		/**
		 * Soll zwischen Blau und Gr�n oder Rot und Gr�n variiert werden? (F�r Farbenschwache)
		 */
		private static boolean blue;
		

		
		/**
		 * Welche Art von Funktion soll gew�hlt werden um den Anteil von Gr�n/Blau und Rot an den Overlays zu bestimmten?
		 */
		private static int functionType = 1;
		
		/**
		 * Der Alphawert der gemalten Overlays
		 */
		private static int alpha = 100;
		
		/**
		 * Der Standart-Konstruktor<br>
		 * Sichere Felder sind gr�n<br>
		 * Gefahrliche Felder sind rot<br>
		 * Alpha = 100
		 */
		public Safety(){
			this(false);
		}
		
		/**
		 * 
		 * @param safeSquares Die Farbe der sicheren Felder
		 * @param dangerousSquares Die Farbe der gefaehrlichen Felder
		 */
		public Safety(boolean setBlue){
			blue = setBlue;
		}
		
		public void paint(Graphics g){
			Map<Double, Set<Square>> safetyLevels = Main.getSafetySquares();
			if(safetyLevels == null){
				return;
			}
			for(Double safetyLevel : safetyLevels.keySet()){
				if(blue)
					// Die Farbe ergibt sich als Mischung aus Rot und Blau
					// => nur Blau bei sicherheit = 1
					// => nur Rot bei sicherheit = 0
					// => Mischung (T�rkis) bei 1 > sicherheit > 0
					g.setColor(new Color( 0, (int) getColourComponent(safetyLevel, 0.5, 255), (int) getColourComponent((1 - safetyLevel), 0.5, 255) , alpha)); 
				else				
					// Die Farbe ergibt sich als Mischung aus Rot und Gr�n
					// => nur Gr�n bei sicherheit = 1
					// => nur Rot bei sicherheit = 0
					// => Mischung (gelb) bei 1 > sicherheit > 0
					g.setColor(new Color((int) getColourComponent((1 - safetyLevel), 0.5, 255), (int) getColourComponent(safetyLevel, 0.5, 255), 0, alpha)); 
				for(Square square : safetyLevels.get(safetyLevel)){
					if(square.isEmpty()){
						Canvas.drawSquare(g, square.getCoords());
					}
				}
			}
		}
		
		/**
		 * 
		 * @param setAlpha Der neue Alphawert der gemalten Overlays
		 */
		public static void setAlpha(int setAlpha){
			alpha = setAlpha;
		}
		
		/**
		 * 
		 * @return Der Alphawert der gemalten Overlays
		 */
		public static int getAlpha(){
			return alpha;
		}
		
		/**
		 * 
		 * @return Soll zwischen Blau und Gr�n oder Rot und Gr�n variiert werden? (F�r Farbenschwache)
		 */
		public boolean getBlue(){
			return blue;
		}
		
		/**
		 * 
		 * @param b Soll zwischen Blau und Gr�n oder Rot und Gr�n variiert werden? (F�r Farbenschwache)
		 */
		public void setBlue(boolean b){
			blue = b;
			this.repaint();
		}
		
		/**
		 * Funktion die verwendet wird, um eine optimale Farbmischung f�r Werte zwischen max und 0 zu erstellen.
		 * Dazu wird garantiert, dass ab einem Anteil von 50% der Sicherheit (sicher oder gef�hrlich) 
		 * die entsprechende Farbe einen Anteil von 100% an der schlie�lichen Farbmischung zur�ckgegeben wird.
		 * So werden Werte von 0 bis sMax auf eine Strecke von 0 bis max gestreckt und f�r jeden Wert gr��er sMax max zur�ckgegeben wird.
		 * Zudem gibt es daf�r zwei verschiedene Methoden.
		 * <ol>
		 * <li>Es wird simpel alles < sMax mal max/sMax genommen und zur�ckgegeben und f�r alles > sMax wird max zur�ckgegeben.</li>
		 * <li>Es wird der �bergebene Wert in eine <a href="https://en.wikipedia.org/wiki/Logistic_function" >Logistische Funktion</a> eingesetzt, welche die waagerechten Asymtoten bei 0 und 1 hat und
		 * bei 0 etwa 0 und bei sMax etwa max zur�ckgibt</li>
		 * </ol>
		 */
		public static double getColourComponent ( double safety, double sMax, double max){
			return getColourComponent(functionType, safety, sMax, max);
		}
		
		/**
		 * Funktion die verwendet wird, um eine optimale Farbmischung f�r Werte zwischen max und 0 zu erstellen.
		 * Dazu wird garantiert, dass ab einem Anteil von 50% der Sicherheit (sicher oder gef�hrlich) 
		 * die entsprechende Farbe einen Anteil von 100% an der schlie�lichen Farbmischung zur�ckgegeben wird.
		 * So werden Werte von 0 bis sMax auf eine Strecke von 0 bis max gestreckt und f�r jeden Wert gr��er sMax max zur�ckgegeben wird.
		 * Zudem gibt es daf�r zwei verschiedene Methoden.
		 * <ol>
		 * <li>Es wird simpel alles < sMax mal max/sMax genommen und zur�ckgegeben und f�r alles > sMax wird max zur�ckgegeben.</li>
		 * <li>Es wird der �bergebene Wert in eine <a href="https://en.wikipedia.org/wiki/Logistic_function" >Logistische Funktion</a> eingesetzt, welche die waagerechten Asymtoten bei 0 und 1 hat und
		 * bei 0 etwa 0 und bei sMax etwa max zur�ckgibt</li>
		 * </ol>
		 */
		public static double getColourComponent ( int functionType, double safety, double sMax, double max){
			switch(functionType){
			case 2:
				//Logistische Funktion
				//(Hebt die Unterschiede st�rker hervor)
				return max/(1 + Math.pow(Math.E, (-20*(safety-sMax/2))));
			default:
			case 1: 
				//Lineare Funktion
				if(safety >= sMax){
					return max;
				}
				else return safety * (max/sMax);
			}
		}

		/**
		 * @return Welche Art von Funktion soll gew�hlt werden um den Anteil von Gr�n/Blau und Rot an den Overlays zu bestimmten?<br>
		 * <ol>
		 * <li>Lineare Verteilung</li>
		 * <li>Verteilung anhand einer logistischen Verteilung</li>
		 * </ol>
		 */
		public static int getFunctionType() {
			return functionType;
		}

		/**
		 * Welche Art von Funktion soll gew�hlt werden um den Anteil von Gr�n/Blau und Rot an den Overlays zu bestimmten?<br>
		 * @param functionType <ol>
		 * <li>Lineare Verteilung</li>
		 * <li>Verteilung anhand einer logistischen Verteilung</li>
		 * </ol>
		 */
		public static void setFunctionType(int functionType) {
			int[] possibilities = { 1, 2 };
			// Liegt die �bergebene Zahl im Bereich der M�glichkeiten?
			if(functionType >= possibilities[0] && functionType <= possibilities[possibilities.length - 1])
				Safety.functionType = functionType;
			else
				throw new IllegalArgumentException(functionType + " ist keine m�gliche Funktion.");
			
		}
	}
}
