package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import computation.World;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.SystemColor;
import javax.swing.BoxLayout;
import java.awt.Toolkit;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;

public class ReadWorld extends JDialog {
	
	private static final long serialVersionUID = 1745653542432L;

	/**
	 * Die EditorPane, die die Textrepräsentation des Feldes enthält
	 */
	private JEditorPane dtrpne;
	/**
	 * Die Textrepräsentation des Feldes
	 */
	private String squareTable;
	
	/**
	 * Das Parentframe
	 */
	private AppWindow parent;
	private JButton okButton;
	private JButton cancelButton;
	
	/**
	 * Open the dialogue
	 */
	public void setVisible(boolean b) {
		super.setVisible(b);
	}
	
	/**
	 * Set the parent Frame
	 */
	public void setParent(AppWindow window){
		parent = window;
	}

	/**
	 * Create the dialog.
	 */
	public ReadWorld(AppWindow window) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ReadWorld.class.getResource("/icon.png")));
		setTitle("YamYam Welteingabe");
		setModal(true);
		setBounds(100, 100, 512, 339);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		this.setParent(window);
		ReadWorld self = this;
		
		dtrpne = new JEditorPane();
		dtrpne.setToolTipText("Geben sie hier das darzustellende Square mit ASCII-Zeichen ein");
		getContentPane().add(dtrpne, BorderLayout.CENTER);
		dtrpne.setFont(new Font("OCR A Extended", Font.PLAIN, 15));
		dtrpne.setText("############\r\n# #      # #\r\n#   ##     #\r\n#   #   E# #\r\n#       ## #\r\n#          #\r\n# #E    E###\r\n############\n");
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int error = World.worldIsConsistent(dtrpne.getText());
						if(error == 0){
							squareTable = dtrpne.getText();
							World.setBrett(squareTable);
							parent.setFeldDialogVisible(false);
							parent.visualizeBrett();
						}
						else{
							JOptionPane.showMessageDialog(self,
								    "Das Square ist nicht vollständig:\n"+
								    World.worldIsConsistent(error),
								    "Fehler",
								    JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				okButton.setActionCommand("OK");
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Abbruch");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						parent.setFeldDialogVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
			}
			buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
			JPanel panel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel.getLayout();
			flowLayout.setAlignOnBaseline(true);
			panel.setBackground(SystemColor.info);
			JLabel lblWand = new JLabel("\"#\" = Wand");
			panel.add(lblWand);
			JLabel lblEAusgang = new JLabel("\"E\" = Ausgang");
			panel.add(lblEAusgang);
			JLabel label = new JLabel("\" \" = Leeres Square");
			panel.add(label);
			buttonPane.add(panel);
			buttonPane.add(okButton);
			
			JButton btnExamples = new JButton("Beispiele");
			btnExamples.setToolTipText("Beispielwelten, bereitgestellt von bundeswettbewerb-informatik.de");
			buttonPane.add(btnExamples);
			
			JPopupMenu examplesMenu = new JPopupMenu();
			addPopup(btnExamples, examplesMenu);
			
			JMenuItem mntmExample0 = new JMenuItem("Beispiel 0");
			mntmExample0.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("############\r\n# #      # #\r\n#   ##     #\r\n#   #   E# #\r\n#       ## #\r\n#          #\r\n# #E    E###\r\n############\n");
				}
			});
			examplesMenu.add(mntmExample0);
			
			JMenuItem mntmExample1 = new JMenuItem("Beispiel 1");
			mntmExample1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("########################\r\n#    #    #    #       #\r\n#    #    #    #       #\r\n#         #        E  ##\r\n#                      #\r\n#              #       #\r\n#    ##  ##    #       #\r\n#    #    #    #   #   #\r\n#    #  E #    #       #\r\n#   E######    #       #\r\n#    #    #    #       #\r\n#    #    #    #       #\r\n########################\n");
				}
			});
			examplesMenu.add(mntmExample1);
			
			JMenuItem mntmExample2 = new JMenuItem("Beispiel 2");
			mntmExample2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("###################################################\r\n# #   #               #         #   #             #\r\n# ### # ########### # # ##### # ### # ### ####### #\r\n#   # #     #   #   # #   # # #   # #   # #       #\r\n### # ##### # # # ### ### # # ### # ### # ####### #\r\n#E# #         # # # # #     # # # #     #       # #\r\n# # ### ######### # # ##### # # # # ########### # #\r\n# #   # # #     # # #     # # # # #       #   # # #\r\n# ### # # # ### # # ##### ### # # ##### ### # # # #\r\n#   # #   #   # # #     #   # # #     # #   #   # #\r\n### # ### ### # # # ####### # # ##### ### ####### #\r\n#   #   # #   #   #   #     # #     #   # #       #\r\n# # ### ### ####### # # ##### # # ##### # # #######\r\n# #   #   #       # #   #     # # #   #   # #E    #\r\n# ####### ### ### # ##### # ### ### # ##### ### ###\r\n# #     #   #   # #     # # #       #     #   #   #\r\n# # ### ### ##### ##### # # # ####### ### ### ### #\r\n# #   #   # #   #   #   #E# #   #   # # #   #     #\r\n# ### # ### # # # # # ##### ### # ### # ### ##### #\r\n#     #     # # # # # #     #   # #   #     #     #\r\n# ########### # ### # # ##### ### # ######### #####\r\n# #           #   # #   #   #     #     #   #   # #\r\n# ##### ######### # ##### # ##### ##### # # ### # #\r\n#       #           #     #           #   #       #\r\n###################################################\n");
				}
			});
			examplesMenu.add(mntmExample2);
			
			JMenuItem mntmExample3 = new JMenuItem("Beispiel 3");
			mntmExample3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("###################\r\n#    #            #\r\n##                #\r\n#         #       #\r\n#                ##\r\n#          ###    #\r\n#    #            #\r\n#                 #\r\n#                 #\r\n#      #        E #\r\n##     #          #\r\n#   ####    #     #\r\n#   #             #\r\n#   #             #\r\n###################\n");
				}
			});
			examplesMenu.add(mntmExample3);
			
			JMenuItem mntmExample4 = new JMenuItem("Beispiel 4");
			mntmExample4.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("###################\r\n#    #      #     #\r\n##   #            #\r\n#         ##      #\r\n#    #            #\r\n#    #     ###    #\r\n#  E #####        #\r\n#      #          #\r\n#      #          #\r\n#      #####      #\r\n##     #          #\r\n#   ####    #######\r\n#   #             #\r\n#   #    #        #\r\n###################\r\n");
				}
			});
			examplesMenu.add(mntmExample4);
			
			JMenuItem mntmExample5 = new JMenuItem("Beispiel 5");
			mntmExample5.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("########################\r\n#              #       #\r\n#              # ###   #\r\n#              ###     #\r\n#      ### ##          #\r\n#    ###    #          #\r\n#    #    # #    #     #\r\n#    #  ### #    #     #\r\n#### #      #   ###    #\r\n#E       ####    #     #\r\n####  #        # #  #  #\r\n#     #     ###E    #  #\r\n#     #      #    ###  #\r\n#     ###    #         #\r\n#   E           ###    #\r\n#           ##    #    #\r\n#####   #    #    #    #\r\n#   #   ##   #         #\r\n#   #      #    #      #\r\n#   # #### ###  #      #\r\n#        #      ###    #\r\n#              E       #\r\n#                      #\r\n########################\n");
				}
			});
			examplesMenu.add(mntmExample5);
			
			JMenuItem mntmExample6 = new JMenuItem("Beispiel 6");
			mntmExample6.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dtrpne.setText("######\r\n#E## #\r\n#    #\r\n##   #\r\n#  # #\r\n# #  #\r\n#    #\r\n##   #\r\n#  # #\r\n# #  #\r\n#    #\r\n##   #\r\n#  # #\r\n# #  #\r\n#    #\r\n######\r\n");
				}
			});
			examplesMenu.add(mntmExample6);
			buttonPane.add(cancelButton);
		}
	}

	/**
	 * @return Die Textrepräsentation des Feldes
	 */
	public String getWorld() {
		return squareTable;
	}

	/**
	 * @param world Die neue Textrepräsentation des Feldes
	 */
	public void setWorld(String world) {
		squareTable = world;
		dtrpne.setText(world);
	}
	
	/**
	 * Das Popup beim Beispielbutton
	 * @param component
	 * @param popup
	 */
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				showMenu(e);
			}
			public void mouseReleased(MouseEvent e) {
				showMenu(e);
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
