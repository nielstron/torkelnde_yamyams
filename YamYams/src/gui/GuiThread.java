package gui;

public class GuiThread implements Runnable {

	@Override
	public void run() {
		try {
			AppWindow frame = new AppWindow(this);
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
