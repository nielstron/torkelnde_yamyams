package computation;

import java.awt.Point;

public class YamYam {
	
	/**
	 * Die Koordinaten auf denen das YamYam steht
	 */
	private static Point coords = new Point(1,1);

	/**
	 * @return Das Square auf dem das YamYam steht
	 */
	public static Square getSquare() {
		return World.getSquareAt(coords);
	}

	/**
	 * @param standfeld Das Square auf dem das YamYam stehen soll
	 */
	public static void setSquare(Square curSquare) {
		YamYam.coords = curSquare.getCoords();
	}
	
	/**
	 * @return Die Koordinaten des YamYams
	 */
	public static Point getCoords(){
		return coords;
	}
	
	/**
	 * @param coords Die neuen Koordinaten des YamYams
	 */
	public static void setCoords(Point coords){
		if(coords == null) throw new NullPointerException();
		YamYam.coords = coords;
	}
}
