package computation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SafetyOfAllSquares extends Thread {
	
	/**
	 * Die Sicherheit aller Felder in der Zuordnung Sicherheit->Felder
	 */
	private Map<Double, Set<Square>> safetySquares;
	
	/**
	 * Die gef�hrlichen Felder
	 */
	private Set<Square> dangerousSquares;
	
	/**
	 * Die sicheren Felder
	 */
	private Set<Square> safeSquares;
	
	/**
	 * Soll, und mit welcher Genauigkeit, berechnet werden wie gro� die Wahrscheinlichkeit
	 * bei Feldern ist, welche keine absolute Wahrscheinlichkeit aufweisen?
	 */
	private int accuracy;
	
	/**
	 * Der aktuelle Fortschritt des Programmes
	 */
	private double progress;
	
	/**
	 * Standartkonstruktor
	 */
	public SafetyOfAllSquares(){
		this(0);
	}
	
	/**
	 * Konstruktor
	 * @param accuracy Soll, und mit welcher Genauigkeit, berechnet werden wie gro� die Wahrscheinlichkeit
	 * bei Feldern ist, welche keine absolute Wahrscheinlichkeit aufweisen?
	 */
	public SafetyOfAllSquares(int accuracy){
		this.accuracy = accuracy;
	}
	
	/**
	 * Startet die Methode safetyOfAllSquares mit der beim Konstruktor �bergebenen Genauigkeit
	 */
	public void run(){
		safetyOfAllSquares();
	}
	
	/**
	 * Gibt eine Map zur�ck, welche f�r jedes Feld die Wahrscheinlichkeit enth�lt einen Ausgang zu erreichen
	 * @return Eine Map, in der �ber jede m�gliche Wahrscheinlichkeit auf die Felder verwiesen wird, welche sie aufweisen
	 */
	public Map<Double, Set<Square>> safetyOfAllSquares (){
		return safetyOfAllSquares(accuracy);
	}
	
	/**
	 * Gibt eine Map zur�ck, welche f�r jedes Feld die Wahrscheinlichkeit enth�lt einen Ausgang zu erreichen
	 * @param accuracy Soll, und mit welcher Genauigkeit, berechnet werden wie gro� die Wahrscheinlichkeit
	 * bei Feldern ist, welche keine absolute Wahrscheinlichkeit aufweisen?
	 * @return Eine Map, in der �ber jede m�gliche Wahrscheinlichkeit auf die Felder verwiesen wird, welche sie aufweisen
	 */
	public  Map<Double, Set<Square>> safetyOfAllSquares (int accuracy){
		// Alle sicheren Felder werden auf 1 gemappt, alle unsicheren auf 0.5, alle absolut unsicheren auf 0
		// => Sp�ter: berechnung der (genauen) unsicherheit
		Map<Double, Set<Square>> returnMap = new HashMap<Double, Set<Square>>();
		// Initialisierung
		// Falls es noch kein World gibt, breche ab
		if(World.getWorld() == null) return returnMap;
		// Anfangs alle Felder in der Welt, am Ende alle Felder, die nicht auf unsichere Felder f�hren
		Set<Square> safeSquares = new HashSet<Square>();
		for(Square[] row : World.getWorld()){
			for(Square square : row){
				safeSquares.add(square);
			}
		}
		
		// Alle gef�hrlichen Felder, die also nie zu einem Ausgang f�hren
		Set<Square> dangerousSquares = new HashSet<Square>();
		// Ein Rundenz�hler f�r den Fortschritt
		int i = 0;
		// F�ge alle Felder die nie zu einem Ausgang f�hren in gefaehrlicheFelder ein
		for(Square square : safeSquares){
			boolean reachExit = false;
			for(Square errFeld : square.getYamyamAllReachableSquares()){
				if(errFeld.isExit()){
					reachExit = true;
					break;
				}
			}
			if(!reachExit){
				dangerousSquares.add(square);
			}
			
			//Hier wird der Fortschritt bis 25% hochgez�hlt
			setProgress(((double) (i++) /(double)safeSquares.size()) * 0.25);
		}
		//Entferne alle gef�hrlichen Felder aus dem Set der sicheren Felder
		safeSquares.removeAll(dangerousSquares);
		
		this.dangerousSquares = dangerousSquares;
		
		// Ein Rundenz�hler f�r den Fortschritt
		i = 0;
		// Alle Felder, die auf gef�hrliche Felder f�hren
		Set<Square> unsafeSquares = new HashSet<Square>();
		for(Square square : safeSquares){
			for(Square dangerousSquare : dangerousSquares){
				if(square.getYamyamAllReachableSquares().contains(dangerousSquare)){
					unsafeSquares.add(square);
					break;
				}
			}
			//Hier wird der Fortschritt ab 25% bis 50% hochgez�hlt
			setProgress(((double) (++i) /(double)safeSquares.size()) * 0.25 + 0.25);
		}
		//Entferne alle unsicheren Felder aus dem Set der sicheren Felder
		safeSquares.removeAll(unsafeSquares);
		
		this.safeSquares = safeSquares;
		
		// Alle Felder in safeSquares haben eine Ausgangserreichwarscheinlichkeit von 1
		returnMap.put((double) 1, safeSquares);
		
		setSafetyOfAllSquares(returnMap);
		// Alle restlichen, unsicheren Felder haben eine ungewisse Ausgangserreichwarscheinlichkeit
		// Diese kann f�r eine endliche Anzahl von Runden bestimmt werden 
		
		if(accuracy > 0){
			returnMap.putAll(exitReachProbability(accuracy, unsafeSquares));
		}
		// Bis dahin: Ausgangserreichwarscheinlichkeit ~ 0.5
		else{
			returnMap.put(0.5, unsafeSquares);
		}
		
		// Alle Felder in gefaehrlicheFelder haben eine Ausgangserreichwarscheinlichkeit von 0
		if(returnMap.get(0.0) == null){
			returnMap.put(0.0, new HashSet<Square>());
		}
		returnMap.get(0.0).addAll(dangerousSquares);
		
		setSafetyOfAllSquares(returnMap);
		
		return returnMap;
	}
	
	/**
	 * Berechne, mit der gegebenen Genauigkeit, die Wahrscheinlichkeit f�r jedes �bergebene Feld, auf einem sicheren Feld oder Ausgang zu landen
	 * @param accuracy Die Zahl von Schritten, f�r die die Ausgangserreichwahrscheinlichkeit berechnet wird
	 * @param unsafeSquares Die Felder, deren absolute Ausgangswahrscheinlichkeit unbekannt ist
	 * @return Eine Map, in der �ber jede m�gliche Wahrscheinlichkeit auf die Felder verwiesen wird, welche sie aufweisen
	 */
	public  Map<Double, Set<Square>> exitReachProbability (int accuracy, Set<Square> unsafeSquares){
		//Die Map, die am Ende zur�ckgegeben werden soll
		Map<Double, Set<Square>> returnMap = new HashMap<Double, Set<Square>>();
		
		/*
		 * Versuch 1: Erstelle f�r jedes Feld einen Baum, der alle m�glichen n�chsten Schritte darstellt.
		 * Berechne anschlie�end daraus die Wahrscheinlichkeit ein sicheres Feld oder einen Ausgang zu erreichen
		 *
		int i = 1;
		for(Square unsafeSquare : unsafeSquares){
			//Erstellt einen Baum, der accuracy Schritte darstellt
			WayTree wayTree = new WayTree(unsafeSquare, accuracy);
			//Bestimme damit die Ausgangserreichwahrscheinlichkeit
			double exitReachProbability = wayTree.exitReachProbability();
			if(returnMap.get(exitReachProbability) == null){
				returnMap.put(exitReachProbability, new HashSet<Square>());
			}
			returnMap.get(exitReachProbability).add(unsafeSquare);
			System.out.println( 
					(int) (((double) i / (double)unsafeSquares.size()) * 100) + "% " +
					"Feld "+ i++ + " berechnet: "+ exitReachProbability);
		}
		return returnMap;
		*
		*
		* Versuch 2: Berechne f�r jedes Feld die Wahrscheinlichkeit innerhalb von einem Schritt ein sicheres Feld oder einen Ausgang zu erreichen.
		* Speichere diese und f�hre diesen Schritt weitere accuracy Male aus
		* 
		* Ergebnis: Wesentlich effizienter und schneller.
		* 
		* Erkl�rung: F�r viele Felder wurden bei den Wegb�umes mehrfach (bis zu millionenfach) Knoten mit derselben Wahrscheinlichkeit erstellt
		*/
		//Initialisiere eine Map, die f�r jedes Feld die Ausgangserreichwahrscheinlichkeit speichert
		Map<Square, Double> reachProbMap = new HashMap<Square, Double>();
		//F�ge zuerst alle sicheren und unsicheren Felder ein
		for(Square safe : safeSquares){
			reachProbMap.put(safe, 1.0);
		}
		for(Square dangerous : dangerousSquares){
			reachProbMap.put(dangerous, 0.0);
		}
		//Anfangs ist die Ausgangserreichwahrscheinlichkeit jedes unsicheren Feldes auch 0, da innerhalb von 0 Schritten der Ausgang nicht erreicht wird
		for(Square unsafe : unsafeSquares){
			reachProbMap.put(unsafe, 0.0);
		}
		/*
		 * Debug-Ausgabe: F�r jedes Feld, nach jedem Schritt, die Ausgangserreichwahrscheinlichkeit
		 */
		for(Square debugSquare : unsafeSquares){
			System.out.print(debugSquare.getCoords() + "\t");
		}
		System.out.print("\n");
		// Nun berechne accuracy Mal, wie gro� die Ausgangserreichwahrschienlichkeit innerhalb von einem Schritt ist
		for(int i = 0; i < accuracy; i++){
			//Diese Map wird die neuen Werte f�r jedes Feld speichern und die alte Map anschlie�end ersetzen
			//Sie wird aber ben�tigt, da anhand der alten Werte gerechnet wird, und diese ansonsten verloren gehen w�rden
			Map<Square, Double> tmpReachProbMap = new HashMap<Square, Double>();
			for(Square square : unsafeSquares){
				double oneStepProb = 0.0;
				Set<Square> nextSquares = square.getYamyamAllNextSquares();
				// Die Ausgangserreichwahrscheinlichkeit ist die Wahrscheinlichkeit der n�chsten Felder addiert..
				for(Square nextSquare : nextSquares){
					oneStepProb += (double) reachProbMap.get(nextSquare);
				}
				//..und schlie�lich geteilt durch die Anzahl der n�chsten Felder (LaPlace Wahrscheinlichkeitsverteilung)
				oneStepProb /= nextSquares.size();
				tmpReachProbMap.put(square, oneStepProb);
			}
			// �bertrage schlie�lich die neuen Werte von tmpReachProb nach ReachProb
			reachProbMap.putAll(tmpReachProbMap);
			/* System.out.println( 
					(int)(((double) i / (double)accuracy) * 100) + "% " +
					"Schritt "+ i + " berechnet"
				);*/
			/*
			 * Debug-Ausgabe: F�r jedes Feld, nach jedem Schritt, die Ausgangserreichwahrscheinlichkeit
			 */
			
			for(Square debugSquare : unsafeSquares){
				System.out.print(tmpReachProbMap.get(debugSquare) + "\t");
			}
			System.out.print("\n");
			
			//Der Rechenfortschritt z�hlt ab jetzt ab 50% aufw�rts
			setProgress(((double)i/(double)accuracy ) * 0.5 + 0.5);
			//Und der bisherige Stand wird gespeichert
			setSafetyOfAllSquaresOpposite(tmpReachProbMap);
			
		}
		//Forme schlie�lich die reachProbMap um
		returnMap = convertSafetyMap(reachProbMap);
		
		return returnMap;
	}
	
	/**
	 * Gibt die Sicherheit aller Felder zur�ck
	 * @return Map der Zuordnung Sicherheit->Felder
	 */
	public Map<Double, Set<Square>> getSafetyOfAllSquares(){
		return safetySquares;
	}
	
	/**
	 * Speichert die �bergebene Map als Sicherheit aller Felder ab
	 * @param safetySquares Map der Zuordnung Sicherheit->Felder
	 */
	public void setSafetyOfAllSquares(Map<Double, Set<Square>> safetySquares){
		this.safetySquares = safetySquares;
	}
	
	/**
	 * Speichert die �bergebene Map als Sicherheit aller Felder ab
	 * @param safetySquares Map der Zuordnung Feld->Sicherheit
	 */
	public void setSafetyOfAllSquaresOpposite(Map<Square, Double> safetySquares){
		setSafetyOfAllSquares(convertSafetyMap(safetySquares));
	}
	
	/**
	 * Forme die Map der Zuordnung Feld->Sicherheit nach Sicherheit->Felder um
	 * @param reachProbMap Map der Zuordnung Feld->Sicherheit
	 * @return Map der Zuordnung Sicherheit->Felder
	 */
	public Map<Double, Set<Square>> convertSafetyMap (Map<Square, Double> reachProbMap){
		Map<Double, Set<Square>> returnMap = new HashMap<Double, Set<Square>>();
		//Forme die reachProbMap um
		for(Square unsafe : reachProbMap.keySet()){
			double prob = reachProbMap.get(unsafe);
			if(returnMap.get(prob) == null){
				returnMap.put(prob, new HashSet<Square>());
			}
			returnMap.get(prob).add(unsafe);
		}
		return returnMap;
	}
	
	
	public double getProgress() {
		return progress;
	}

	private void setProgress(double progress) {
		this.progress = progress;
	}


	public int getAccuracy() {
		return accuracy;
	}


	/**
	 * Klasse die einen Baum speichert, der einen zur�ckgelegten Weg verwaltet
	 * @deprecated
	 * @author Niels
	 *
	 */
	public static class WayTree {
		
		/**
		 * Der Startpunkt des zur�ckgelegten Weges
		 */
		private Node root;
		
		/**
		 * Erstellt einen Baum, dessen Startpunkt das �bergebene Feld ist
		 * @param square
		 */
		public WayTree(Square square, int level){
			root = new Node.MiddleNode.UnsafeNode(square, null, level);
		}
		
		/**
		 * 
		 * @return Der Startpunkt des zur�ckgelegten Weges
		 */
		public Node getRoot(){
			return root;
		}
		
		/**
		 * 
		 * @return Die Wahrscheinlichkeit vom Startpunkt aus den Ausgang zu erreichen
		 */
		public double exitReachProbability(){
			return root.exitReachProbability();
		}
		
		/**
		 * Ein Knoten im Baum des zur�ckgegelegten Weges
		 * @author Niels
		 *
		 */
		public static abstract class Node {
			
			/**
			 * Das Feld auf das dieser Knoten verweist
			 */
			protected Square square;
			
			/**
			 * Der Vaterknoten
			 */
			protected Node parent;
			
			/**
			 * Die Knoten, die von diesem Feld aus erreicht werden k�nnen
			 */
			protected Set<Node> nextNodes;
			
			/**
			 * Erzeugt einen Knoten der ein begangenes Feld auf einem Weg darstellt
			 * @param square Das Feld auf das der Knoten verweist
			 * @param parent Der Vaterknoten des Knotens
			 * @param level Die Anzahl von Schritten die noch getan werden soll
			 */
			public Node(Square square, Node parent, int level){
				this.square = square;
				this.parent = parent;
				this.nextNodes = new HashSet<Node>();
			}
			
			public Node(Square square, Node parent){
				this(square, parent, 0);
			}
			
			/*
			 * Laufzeitverbesserung: Eigenen Klassen f�r unsichere Felder, sichere Felder und Ausg�nge und gef�hrliche Felder
			 *  => Verringerung der Abfragen
			 */
			/**
			 * Klasse, die ein "Blatt" am Wegbaum, also dessen letztes Element verwaltet
			 * @author Niels
			 *
			 */
			public static abstract class EndNode extends Node {
				
				public EndNode(Square square, Node parent) {
					super(square, parent, 0);
				}
				
				public static class SafeNode extends EndNode{
					
					public SafeNode(Square square, Node parent){
						super(square, parent);
					}
					
					/**
					 * Gibt die Wahrscheinlichkeit zur�ck, dass von diesem Feld aus ein Ausgang erreicht wird
					 * @return Die Wahrscheinlichkeit, dass von diesem Feld aus ein Ausgang erreicht wird
					 */
					public double exitReachProbability(){
						return 1.0;
					}
					
				}
				
				public static class DangerousNode extends EndNode{
					
					public DangerousNode(Square square, Node parent){
						super(square, parent);
					}
					
					/**
					 * Gibt die Wahrscheinlichkeit zur�ck, dass von diesem Feld aus ein Ausgang erreicht wird
					 * @return Die Wahrscheinlichkeit, dass von diesem Feld aus ein Ausgang erreicht wird
					 */
					public double exitReachProbability(){
						return 0.0;
					}
					
				}
				
			}
			
			public static abstract class MiddleNode extends Node{
				
				public MiddleNode(Square square, Node parent, int level) {
					super(square, parent, level);
				}
				
				public MiddleNode(Square square, Node parent) {
					super(square, parent);
				}
				
				/**
				 * Eine Klasse die alle Felder erstellt, deren Sicherheit nicht bekannt ist
				 * @author Niels
				 *
				 */
				public static class UnsafeNode extends Node{

					public UnsafeNode(Square square, Node parent, int level) {
						super(square, parent, level);
						if(level > 0){
							for(Square reachableSquare : square.getYamyamAllNextSquares()){
								/*if(dangerousSquares.contains(reachableSquare))
									nextNodes.add(new EndNode.DangerousNode(reachableSquare, this));
								else if(safeSquares.contains(reachableSquare))
									nextNodes.add(new EndNode.SafeNode(reachableSquare, this));
								else
									nextNodes.add(new MiddleNode.UnsafeNode(reachableSquare, this, level - 1));*/
							}
						}
					}
					
					public UnsafeNode(Square square, Node parent){
						super(square, parent);
					}
					
					/**
					 * Gibt die Wahrscheinlichkeit zur�ck, dass von diesem Feld aus ein Ausgang erreicht wird
					 * @return Die Wahrscheinlichkeit, dass von diesem Feld aus ein Ausgang erreicht wird
					 */
					public double exitReachProbability(){
						double probability = 0.0;
						for(Node reachableNode : nextNodes){
							probability += reachableNode.exitReachProbability();
						}
						return probability / (double) square.getYamyamAllNextSquares().size();
					}
					
				}
				
			}
			
			/**
			 * Gibt die Wahrscheinlichkeit zur�ck, dass von diesem Feld aus ein Ausgang erreicht wird
			 * @return Die Wahrscheinlichkeit, dass von diesem Feld aus ein Ausgang erreicht wird
			 */
			public abstract double exitReachProbability();
			
			/**
			 * 
			 * @return Das Feld auf das dieser Knoten verweist
			 */
			public Square getSquare(){
				return square;
			}
			
			/**
			 * 
			 * @return Der Vaterknoten
			 */
			public Node getParent(){
				return parent;
			}
			
			/**
			 * 
			 * @return Die Knoten, die von diesem Feld aus erreicht werden k�nnen
			 */
			public Set<Node> getNextNodes(){
				return nextNodes;
			}
		}
		
	}
	
}
