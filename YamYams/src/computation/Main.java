package computation;

import java.awt.EventQueue;
import java.util.Map;
import java.util.Set;

import gui.GuiThread;

public class Main {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new GuiThread());
	}
	
	public static Map<Double, Set<Square>> getSafetySquares() {
		return safetySquares;
	}

	public static void setSafetySquares(Map<Double, Set<Square>> safetySquares) {
		Main.safetySquares = safetySquares;
	}

	private static Map<Double, Set<Square>> safetySquares;

}
