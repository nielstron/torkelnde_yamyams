package computation;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 * Die Klasse zur Speicherung und Verarbeitung der Information �ber die Welt der YamYams
 * @param feld Die Text-Repr�sentation des Brettes
 */
public class World {

	/**
	 * Die Square-Array Repr�sentation der Welt
	 */
	private static Square[][] squareTable;

	/**
	 * @return Die Square-Array-Repr�sentation des Brettes
	 */
	public static Square[][] getWorld() {
		return squareTable;
	}

	/**
	 * @param square Die neue Text-Repr�sentation des Brettes
	 */
	public static void setBrett(String square) {
		squareTable = stringToSquareArray(square);
	}
	
	
	/**
	 * Konvertiere die String-Repr�sentation zu einer Square-Array-Repr�sentation
	 * @return Das World als 2-Dimensionales Square-Array
	 */
	public static Square[][] stringToSquareArray(String square){
		String[] twoDimensionalWorld = square.split("\n");
		Square[][] squareTable = new Square[twoDimensionalWorld.length][];
		
		
		for(int j = 0; j < twoDimensionalWorld.length; j++){
			String row = twoDimensionalWorld[j];
			ArrayList<Square> tableRow = new ArrayList<Square>();
			for(int i = 0; i < row.length(); i++){
				char c = row.charAt(i);
				Point coords = new Point(i, j);
				switch(c){
				case '#':
					tableRow.add(new Square("wall", coords));
					break;
				case 'e':
				case 'E':
					tableRow.add(new Square("exit", coords));
					break;
				case ' ':
					tableRow.add(new Square("empty", coords));
				}
			}
			squareTable[j] = tableRow.toArray(new Square[0]);
		}
		return squareTable;
	}
	
	/**
	 * Teste das Square auf Konsistenz<br>
	 * Dazu geh�ren:<br>
	 * <ul>
	 * <li>In jeder Reihe gleich viele Felder</li>
	 * <li>An den R�ndern Wand-Felder</li>
	 * <ul>
	 * @return Ist das Square konsistent? 
	 * 			0 => Ja, 
	 * 			1 => Die Feldanzahl ist nicht immer gleich gro�, 
	 * 			2 => Oben und Unten sind nicht alle Felder Wandfelder
	 * 			3 => Rechts und Links sind nicht alle Felder Wandfelder
	 */
	public static int worldIsConsistent( Square[][] squareTable){
		//Sind in jeder Reihe gleich viele Felder?
		int i = squareTable[0].length;
		for(Square[] row : squareTable){
			if(row.length != i) return 1;
		}
		//Sind an den R�ndern nur Wand-Felder?
		for(i = 0; i < squareTable.length; i++){
			//�berpr�fe die erste und letzte Reihe Komplett
			if(i == 0 || i == squareTable.length - 1){
				for(Square cell : squareTable[i]){
					if(!cell.getType().equals("wall")) return 2;
				}
			}
			//�berpr�fe in alle anderen Reihen nur das erste und letzte Square
			else{
				if(	!squareTable[i][0].getType().equals("wall") ||
					!squareTable[i][squareTable[i].length - 1].getType().equals("wall")
					)
					return 3;
			}
		}
		
		return 0;		
	}
	
	/**
	 * Teste die statische World-Welt auf Konsistenz<br>
	 * Dazu geh�ren:<br>
	 * <ul>
	 * <li>In jeder Reihe gleich viele Felder</li>
	 * <li>An den R�ndern Wand-Felder</li>
	 * <ul>
	 * @return Ist die Welt konsistent? 
	 * 			0 => Ja, 
	 * 			1 => Die Feldanzahl ist nicht immer gleich gro�, 
	 * 			2 => Oben und Unten sind nicht alle Felder Wandfelder
	 * 			3 => Rechts und Links sind nicht alle Felder Wandfelder
	 */
	public static int worldIsConsistent(){
		return World.worldIsConsistent(World.getWorld());
	}
	
	/**
	 * Teste die Welt auf Konsistenz<br>
	 * Dazu geh�ren:<br>
	 * <ul>
	 * <li>In jeder Reihe gleich viele Felder</li>
	 * <li>An den R�ndern Wand-Felder</li>
	 * <ul>
	 * @return Ist die Welt konsistent? 
	 * 			0 => Ja, 
	 * 			1 => Die Feldanzahl ist nicht immer gleich gro�, 
	 * 			2 => Oben und Unten sind nicht alle Felder Wandfelder
	 * 			3 => Rechts und Links sind nicht alle Felder Wandfelder
	 */
	public static int worldIsConsistent( String squareTable){
		return World.worldIsConsistent(World.stringToSquareArray(squareTable));
	}
	
	/**
	 * Gibt f�r eine Fehlerzahlr�ckgabe eine Textausgabe mit der Bedeutung der Fehlerzahl zur�ck
	 * @param i Die Fehlerzahl, die durch feldIsConsistent zur�ckgegeben wurde
	 * @return Ein String, der die Bedeutung des Fehlers erkl�rt
	 */
	public static String worldIsConsistent(int i){
		switch(i){
		case 0:
			return "Es gibt keinen Fehler";
		case 1:
			return "Die Feldanzahl ist nicht nicht in jeder Reihe gleich gro�.";
		case 2:
			return "Oben und Unten sind nicht alle Felder Wandfelder";
		case 3:
			return "Rechts und Links sind nicht alle Felder Wandfelder";
		default:
			return "Unbekannter Fehler";
		}
	}
	
	/**
	 * Gebe die Position des Feldes auf dem World zur�ck
	 * @param cell Das gesuchte Square
	 * @return Ein Punkt, der die Koordinaten des Feldes hat
	 */
	public static Point getPosition(Square cell){
		for(int i = 0; i < squareTable.length;i++){
			for(int j = 0; j < squareTable[i].length; j++){
				if(cell.equals(squareTable[i][j])) return new Point(j, i);
			}
		}
		return null;
	}
	
	/**
	 * Gebe das Square an der Position auf dem World zur�ck
	 * @param coords Die Position des Feldes
	 */
	public static Square getSquareAt(Point coords){
		if(coords != null)
			return getSquareAt((int) coords.getX(), (int) coords.getY());
		else return null;
	}
	
	/**
	 * Gebe das Square an der Position auf dem World zur�ck
	 * @param x Die x-Koordinate des Feldes
	 * @param y Die y-Koordinate des Feldes
	 * @return Das Square an dieser Position
	 */
	public static Square getSquareAt(int x , int y){
		if(getWorld() == null) return null;
		if(	squareTable.length <= y ||
			squareTable[0].length <= x ||
			x < 0 ||
			y < 0
			) return null;//throw new IllegalArgumentException("(" + x + " | " + y + " ) liegt au�erhalb des Brettes");
		else{
			return squareTable[y][x];
		}
	}
	
	/**
	 * Gibt eine Map zur�ck, welche f�r jedes Feld die Wahrscheinlichkeit enth�lt einen Ausgang zu erreichen
	 * @param accuracy Soll, und mit welcher Genauigkeit, berechnet werden wie gro� die Wahrscheinlichkeit
	 * bei Feldern ist, welche keine absolute Wahrscheinlichkeit aufweisen?
	 * @return Eine Map, in der �ber jede m�gliche Wahrscheinlichkeit auf die Felder verwiesen wird, welche sie aufweisen
	 */
	public static Map<Double, Set<Square>> safetyOfAllSquares (int accuracy){
		SafetyOfAllSquares safety = new SafetyOfAllSquares();
		return safety.safetyOfAllSquares(accuracy);
	}
}
