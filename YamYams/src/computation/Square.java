package computation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

/**
 * Ein Feld ("Square") in der Welt der YamYams
 * 
 * @author Niels
 *
 */
public class Square {
	
	/**
	 * Die Felddimensionen
	 */
	private static Dimension squareDimensions = new Dimension(20, 20);
	
	/**
	 * Alle m�glichen Feldtypen
	 */
	private static String[] types = 
		{ "wall", "empty", "exit" };
	/**
	 * Der Feldtyp des aktuellen Objekts
	 */
	private String type;
	
	/**
	 * Die Koordinaten des Feldes
	 */
	private Point coords;
	
	/**
	 * Alle direkten Nachbarn des Feldes (NICHT BEI KONSTRUKTION GESPEICHERT)
	 */
	private Square[] neighbors;
	
	/**
	 * Alle f�r ein YamYam direkt erreichbaren Felder (NICHT BEI KONSTRUKTION GESPEICHERT)
	 */
	private Set<Square> yamyamAllNextSquares;
	
	/**
	 * Der Standart Konstruktor
	 */
	public Square(){
		this("empty", 0, 0);
	}
	
	/**
	 * 
	 * @param type Der Typ des zu erzeugenden Feldes ("wall", "empty" oder "exit")
	 * @param x Die x-Koordinate des Feldes
	 * @param y Die y-Koordinate des Feldes
	 */
	public Square(String type, int x, int y){
		this(type, new Point(x, y));
	}
	
	/**
	 * @param type Der Typ des zu erzeugenden Feldes ("wall", "empty" oder "exit")
	 * @param coords Die Koordinaten des Feldes
	 */
	public Square(String type, Point coords){
		if(Square.stringIsPartOfArray(types, type))
			this.setType(type);
		else
			throw new IllegalArgumentException(type + "ist kein m�glicher Typ");
		this.setCoords(coords);
		this.neighbors = null;
		this.yamyamAllNextSquares = null;
	}
	
	/**
	 * Ist der String in dem Array enthalten?
	 * From http://www.programcreek.com/2014/04/check-if-array-contains-a-value-java/
	 * @param arr Dass zu durchsuchende Array
	 * @param targetValue Der gesuchte Wert
	 * @return Ist der Wert enthalten?
	 */
	public static boolean stringIsPartOfArray(String[] arr, String targetValue) {
		for(String s: arr){
			if(s.equals(targetValue))
				return true;
		}
		return false;
	}

	/**
	 * @return Der Typ
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type Der neue Typ
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 
	 * @return Die Felddimensionen
	 */
	public static Dimension getSquareDimensions(){
		return squareDimensions;
	}
	
	/**
	 * Gibt die Farbe zur�ck in der die Zelle gemalt werden soll
	 */
	public Color getColor(){
		switch(this.type){
		case "wall":
			return new Color(0, 0, 0);
		case "exit":
			return new Color(0, 255, 0);
		case "empty":
		default:
			return new Color(255, 255, 255);
		}
	}

	/**
	 * @return Die Koordinaten des Feldes
	 */
	public Point getCoords() {
		return coords;
	}

	/**
	 * @param coords Die neuen Koordniaten des Feldes
	 */
	public void setCoords(Point coords) {
		this.coords = coords;
	}
	
	/**
	 * Gibt den dem �bergebenen char entsprechenden integer zur�ck
	 * @param c Der einer Richtung entsprechende Character (n = Norden, o = Osten, s = S�den, w = Westen)
	 * @return Die entsprechende Zahl  (0 = Norden, 1 = Osten, 2 = S�den, 3 = Westen)
	 */
	public static int dirCharToInt(char c){
		switch(c){
		case 'n':
		case 'N':
			return 0;
		case 'e':
		case 'E':
		case 'o':
		case 'O':
			return 1;
		case 's':
		case 'S':
			return 2;
		case 'w':
		case 'W':
			return 3;
		default:
			throw new IllegalArgumentException(c + " ist keine m�gliche Richtung");
		}
	}
	
	/**
	 * Gibt den dem �bergebenen integer entsprechenden character zur�ck
	 * @param i Die einer Richtung entsprechende Zahl (0 = Norden, 1 = Osten, 2 = S�den, 3 = Westen)
	 * @return Die entsprechende Ziffer f�r die Richtung  (n = Norden, o = Osten, s = S�den, w = Westen)
	 */
	public static char dirIntToChar(int i){
		switch(i){
		case 0:
			return 'n';
		case 1:
			return 'e';
		case 2:
			return 's';
		case 3:
			return 'w';
		default:
			throw new IllegalArgumentException(i + " ist keine m�gliche Richtung");
		}
	}
	
	/**
	 * Gibt die entgegengesetzte Richtung zur �bergegebenen Richtung zur�ck
	 * @param i Die Richtung deren Gegenrichtung gesucht wird
	 * @return Die entgegesetzte Richtung
	 */
	public static int dirOpposite(int i){
		switch(i){
		case 0:
			return 2;
		case 1:
			return 3;
		case 2:
			return 0;
		case 3:
			return 1;
		default:
			throw new IllegalArgumentException(i + " ist keine m�gliche Richtung");
		}
	}
	
	/**
	 * Gibt die entgegengesetzte Richtung zur �bergegebenen Richtung zur�ck
	 * @param c Die Richtung deren Gegenrichtung gesucht wird
	 * @return Die entgegesetzte Richtung
	 */
	public static char dirOpposite(char c){
		return dirIntToChar(dirOpposite(dirCharToInt(c)));
	}
	
	/**
	 * Gibt den Feldnachbar in der �bergebenen Richtung zur�ck
	 * @param dir Die Richtung (0 = Norden, 1 = Osten, 2 = S�den, 3 = Westen)
	 * @return Das n�chste Square in der �bergebenen Richtung, <code>null<code> falls Randfeld
	 */
	public Square getNeighbor(int dir){
		if(dir < 0 || dir > 3) throw new IllegalArgumentException(dir + " ist keine m�gliche Richtung");
		if(neighbors == null){
			//Bestimme einmal s�mtliche Nachbarfelder
			getNeighbors();
		}
		return neighbors[dir];
	}
	
	/**
	 * Gibt alle Nachbarfelder zur�ck
	 * @return Alle eine Koordinate entfernten Felder, nach Richtung sortiert
	 */
	public Square[] getNeighbors(){
		if(neighbors == null){
			neighbors = new Square[4];
			for(int dir = 0; dir < 4; dir ++){
				int neighborX = (int) coords.getX();
				int neighborY = (int) coords.getY();
				switch(dir){
					case 0:
						neighborY++;
						break;
					case 1:
						neighborX++;
						break;
					case 2:
						neighborY--;
						break;
					case 3:
						neighborX--;
						break;
				}
				neighbors[dir] = World.getSquareAt(neighborX, neighborY);
			}
		}
		return neighbors;
	}
	
	/**
	 * Gibt den Feldnachbar in der �bergebenen Richtung zur�ck
	 * @param dir Die Richtung (n = Norden, o = Osten, s = S�den, w = Westen)
	 * @return Das n�chste Square in der �bergebenen Richtung, <code>null<code> falls Randfeld
	 */
	public Square neighbor(char dir){
		return this.getNeighbor(dirCharToInt(dir));
	}
	
	/**
	 * Gibt das Feld zur�ck, welches ein YamYam erreicht, wenn es sich in die �bergebene Richtung bewegt
	 * @param dir Die Richtung die das YamYam einschl�gt
	 * @return Das Ankunftsfeld
	 */
	public Square getYamyamNextSquare(int dir){
		if(dir < 0 || dir > 3) throw new IllegalArgumentException(dir + " ist keine m�gliche Richtung");
		if(!this.isEmpty()) return this;
		Square square = this;
		while(square.isEmpty() && !square.getNeighbor(dir).isWall()){
			square = square.getNeighbor(dir);
		}
		return square;
	}
	
	/**
	 * Gibt alle Felder zur�ck, die ein YamYam von diesem Feld aus direkt erreichen k�nnte
	 * @return Alle direkt erreichbaren Felder
	 */
	public Set<Square> getYamyamAllNextSquares(){
		//Bestimme alle erreichbaren Felder, falls das noch nicht geschehen ist
		if(yamyamAllNextSquares == null){
			yamyamAllNextSquares = new HashSet<Square>();
			for(int i = 0; i < 4; i++){
				Square newFeld = this.getYamyamNextSquare(i);
				yamyamAllNextSquares.add(newFeld);
			}
		}
		return yamyamAllNextSquares;
	}
	
	/**
	 * Gibt alle f�r ein YamYam von hier aus erreichbaren Felder zur�ck
	 * @return Alle f�r ein YamYam erreichbaren Felder
	 */
	public Set<Square> getYamyamAllReachableSquares(){
		// Die noch nicht �berpr�ften Felder
		Set<Square> queue = new HashSet<Square>();
		// Alle Felder die bisher �berpr�ft wurden
		Set<Square> found = new HashSet<Square>();
		queue.add(this);
		while(!queue.isEmpty()){
			Set<Square> removeQueue = new HashSet<Square>();
			Set<Square> addQueue = new HashSet<Square>();
			for(Square square : queue){
				if(found.add(square)){
					addQueue.addAll(square.getYamyamAllNextSquares());
				}
				removeQueue.add(square);
			}
			queue.addAll(addQueue);
			queue.removeAll(removeQueue);
		}
		return found;
	}
	
	/**
	 * 
	 * @return Ist das Feld eine Wand?
	 */
	public boolean isWall(){
		if(type.equals("wall")) return true;
		else return false;
	}
	
	/**
	 * 
	 * @return Ist auf dem Feld ein Ausgang?
	 */
	public boolean isExit(){
		if(type.equals("exit")) return true;
		else return false;
	}
	
	/**
	 * 
	 * @return Ist das Feld leer?
	 */
	public boolean isEmpty(){
		if(type.equals("empty")) return true;
		else return false;
	}
	
	public String toString(){
		return "Square[" + this.type + "," + (int) this.coords.getX() + "," + (int) this.coords.getY() + "]";
	}
}
